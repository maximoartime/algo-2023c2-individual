package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        for (int i = 1; i<fragments.length; i++) {
            int j = i-1;
            Fragment elem = fragments[i];
            while (j>=0 && fragments[j].compareTo(elem) == 1) {
                fragments[j+1] = fragments[j];
                j = j-1;

            }
            fragments[j+1] = elem;
        }
        return fragments;
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        Router[] res = new Router[k];
        Router[] heap = new Router[routers.length];

        heap = routers.clone();

        Heapify.heapify(heap);

        int indice = 0;

        while (indice < k){
            Router max = heap[0];
            if (max.getTrafico() >= umbral) {
                res[indice] = max;
            }
            heap[0] = new Router(0,0);
            Heapify.heapify(heap);
            indice++;
        }

        return res;
    }


    public IPv4Address[] sortIPv4(String[] ipv4) {
        IPv4Address[] ipAddressArray = new IPv4Address[ipv4.length];

        // Convert the array of strings to an array of IPv4Address objects
        for (int i = 0; i < ipv4.length; i++) {
            ipAddressArray[i] = new IPv4Address(ipv4[i]);
        }

        // Apply Radix Sort
        IPv4AddressRadixSort.radixSort(ipAddressArray);

        return ipAddressArray;
    }

}
