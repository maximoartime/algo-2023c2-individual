package aed;

import java.util.Arrays;

public class IPv4AddressRadixSort {
    public static void radixSort(IPv4Address[] addresses) {
        // Perform radix sort on each octet from right to left
        for (int i = 3; i >= 0; i--) {
            countingSort(addresses, i);
        }
    }

    private static void countingSort(IPv4Address[] addresses, int octetIndex) {
        final int RADIX = 256; // IPv4 octet values range from 0 to 255
        int[] count = new int[RADIX];
        IPv4Address[] result = new IPv4Address[addresses.length];

        // Count occurrences of each octet value
        for (IPv4Address address : addresses) {
            int octetValue = address.getOctet(octetIndex);
            count[octetValue]++;
        }

        // Update count array to store the position of each value in the sorted order
        for (int i = 1; i < RADIX; i++) {
            count[i] += count[i - 1];
        }

        // Build the result array by placing elements in their sorted order
        for (int i = addresses.length - 1; i >= 0; i--) {
            int octetValue = addresses[i].getOctet(octetIndex);
            result[count[octetValue] - 1] = addresses[i];
            count[octetValue]--;
        }

        // Copy the result array back to the original array
        System.arraycopy(result, 0, addresses, 0, addresses.length);
    }
}



