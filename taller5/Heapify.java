package aed;
import java.util.*;

public class Heapify {

    public static void heapify(Router[] routers) {
        int n = routers.length;

        // Start from the last non-leaf node and heapify each node in reverse order
        for (int i = n / 2 - 1; i >= 0; i--) {
            heapifyDown(routers, i, n);
        }
    }

    public static Router extractMax(Router[] routers) {
        int n = routers.length;

        if (n == 0) {
            return null; // Heap is empty
        }

        Router max = routers[0]; // Maximum element is at the root

        // Move the last node to the root
        routers[0] = routers[n - 1];

        // Reduce the size of the heap
        n--;

        // Bubble up the new root to its correct position
        heapifyUp(routers, 0, n);

        return max;
    }

    private static void heapifyUp(Router[] routers, int i, int n) {
        while (i < n) {
            int left = 2 * i + 1;
            int right = 2 * i + 2;
            int largest = i;

            // Compare with left child
            if (left < n && routers[left].getTrafico() > routers[largest].getTrafico()) {
                largest = left;
            }

            // Compare with right child
            if (right < n && routers[right].getTrafico() > routers[largest].getTrafico()) {
                largest = right;
            }

            // If largest is not the root, swap and continue bubbling up
            if (largest != i) {
                swap(routers, i, largest);
                i = largest;
            } else {
                break; // Heap property is satisfied
            }
        }
    }


    private static void heapifyDown(Router[] routers, int i, int n) {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        // Compare with left child
        if (left < n && routers[left].getTrafico() > routers[largest].getTrafico()) {
            largest = left;
        }

        // Compare with right child
        if (right < n && routers[right].getTrafico() > routers[largest].getTrafico()) {
            largest = right;
        }

        // If largest is not the root, swap and recursively heapify the affected sub-tree
        if (largest != i) {
            swap(routers, i, largest);
            heapifyDown(routers, largest, n);
        }
    }

    private static void swap(Router[] routers, int i, int j) {
        Router temp = routers[i];
        routers[i] = routers[j];
        routers[j] = temp;
    }
}
