package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    private Nodo raiz;
    private int cardinal;

    private class Nodo {
        // Agregar atributos privados del Nodo
        Nodo izquierdo;
        Nodo derecho;
        T valor;
        Nodo padre;
        
        // Crear Constructor del nodo
        Nodo(T v) {valor = v;
                   izquierdo = null;
                 derecho = null;
           padre = null; }
    }

    public ABB() {
        raiz = null;
        cardinal = 0;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        Nodo indice = raiz;
        Nodo hoja = raiz;
        if(cardinal == 0){
            return null;
        }
        if(cardinal == 1) {
            return raiz.valor;
        }
        while(indice != null){
            hoja = indice;
            indice = indice.izquierdo;
        }
        return hoja.valor;
        
    }

    public T maximo(){
        Nodo indice = raiz;
        Nodo hoja = raiz;
        if(cardinal == 0){
            return null;
        }
        if(cardinal == 1) {
            return raiz.valor;
        }
        while(indice != null){
            hoja = indice;
            indice = indice.derecho;
        }
        return hoja.valor;
    }

    public void insertar(T elem){
        if (pertenece(elem)) {
            return; // Element already exists, nothing to insert
        }

        Nodo insertar = new Nodo(elem);
        if (raiz == null) {
            raiz = insertar;
        } else {
            Nodo busqueda = raiz;
            Nodo hoja;
            while (true) {
                hoja = busqueda;
                if (elem.compareTo(busqueda.valor) > 0) {
                    busqueda = busqueda.derecho;
                } else {
                    busqueda = busqueda.izquierdo;
                }
                if (busqueda == null) {
                    break; // Found the position to insert
                }
            }
            if (elem.compareTo(hoja.valor) > 0) {
                hoja.derecho = insertar;
            } else {
                hoja.izquierdo = insertar;
            }
            insertar.padre = hoja;
        }

        cardinal++;
    }
    

    public boolean pertenece(T elem){
        if (raiz == null) {
            return false;
        } else {
            Nodo busqueda = raiz;
            while (busqueda != null) {
                int compararResultado = elem.compareTo(busqueda.valor);
                if (compararResultado == 0) {
                    return true;
                } else if (compararResultado > 0) {
                    busqueda = busqueda.derecho;
                } else {
                    busqueda = busqueda.izquierdo;
                }
            }
            return false;
        }
    }


    public void eliminar(T elem) {
        while (pertenece(elem)) {
            Nodo eliminarNodo = buscarNodo(elem);
            eliminarUnicoNodo(eliminarNodo);
            cardinal--;
        }
    }

    private void eliminarUnicoNodo(Nodo nodo) {
        Nodo padre = nodo.padre;

        if (nodo.izquierdo == null && nodo.derecho == null) {
            // Case 1: Node has no children
            if (padre == null) {
                raiz = null; // Node is the root
            } else if (padre.izquierdo == nodo) {
                padre.izquierdo = null;
            } else {
                padre.derecho = null;
            }
        } else if (nodo.izquierdo == null) {
            // Case 2: Node has only a right child
            if (padre == null) {
                raiz = nodo.derecho;
            } else if (padre.izquierdo == nodo) {
                padre.izquierdo = nodo.derecho;
            } else {
                padre.derecho = nodo.derecho;
            }
            nodo.derecho.padre = padre;
        } else if (nodo.derecho == null) {
            // Case 2: Node has only a left child
            if (padre == null) {
                raiz = nodo.izquierdo;
            } else if (padre.izquierdo == nodo) {
                padre.izquierdo = nodo.izquierdo;
            } else {
                padre.derecho = nodo.izquierdo;
            }
            nodo.izquierdo.padre = padre;
        } else {
            // Case 3: Node has two children
            Nodo sucesor = primerSucesor(nodo);
            eliminarUnicoNodo(sucesor);
            nodo.valor = sucesor.valor;
        }
    }

    private Nodo primerSucesor(Nodo node) {
        Nodo a = node.izquierdo;
        while (a.derecho != null) {
            a = a.derecho;
        }
        return a;
    }
    private Nodo buscarNodo(T elem) {
        Nodo nodo = raiz;

        while (nodo != null) {
            int compararResultado = elem.compareTo(nodo.valor);

            if (compararResultado == 0) {
                return nodo; // Node found
            } else if (compararResultado < 0) {
                nodo = nodo.izquierdo; // Value is in the left subtree
            } else {
                nodo = nodo.derecho; // Value is in the right subtree
            }
        }

        return null; // Element not found
    }


    public String toString(){
        String res = "{";
        StringBuilder constructor = new StringBuilder(res);
        ABB_Iterador iterador = new ABB_Iterador();
        
        while(iterador.haySiguiente()){
            T value = iterador.siguiente();
            constructor.append(value.toString());
            constructor.append(",");
        }
        constructor.deleteCharAt(constructor.length()-1);
        constructor.append("}");
        return constructor.toString();
        
    }

    private class ABB_Iterador implements Iterador<T> {  // voy a iterar de menor a mayor
        private Nodo _actual;
        private Stack<Nodo> stack;

        ABB_Iterador() {
            _actual = raiz;
            stack = new Stack<>();
            pushLeftNodes(raiz);
        }

        public boolean haySiguiente() {            
            return !stack.isEmpty();
        }
    
        public T siguiente() {
            Nodo node = stack.pop();
            pushLeftNodes(node.derecho);
    
            return node.valor;
        }

        private void pushLeftNodes(Nodo node) {
            while (node != null) {
                stack.push(node);
                node = node.izquierdo;
            }
        }

    }
    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
