package aed;

class Funciones {
    int cuadrado(int x) {
        return x*x;
    }

    double distancia(double x, double y) {
        double res = Math.sqrt(x*x+y*y);
        return res;
    }

    boolean esPar(int n) {
        int res = n % 2;
        if (res == 0) {
            return true;
        }
        else {return false;}
        
    }

    boolean esBisiesto(int año) {
        if ((año%100 != 0 && año%4 == 0) || año%400 == 0) {
            return true;
        }
        else {return false;}
    }

    int factorialIterativo(int n) {
        int res = 1;
        for(int i = 1; i <= n; i++ ) {
            res = i * res;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res = 1;
        while (n>1) {
            res = n * res;
            n = n-1;
        }
        return res;
    }

    boolean esPrimo(int n) {
        int divisores = 0;
        for (int i = 1; i<n+1; i++ ) {
            if (n%i == 0) {
                divisores = divisores+1;
            }
        }
        if (divisores == 2) {
            return true;
        }
        else {return false;}
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int i=0; i < numeros.length; i++) {
            res = res + numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int res = 0;
        for (int i=0; i < numeros.length; i++) {
            if (numeros[i]==buscado) {
                res = i;
            }
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        for (int i=0; i < numeros.length; i++) {
            if (esPrimo(numeros[i])) {
                return true;
            }
        }  
        return false;
    }

    boolean todosPares(int[] numeros) {
        int cantidad = 0;
        for (int i=0; i < numeros.length; i++) {
            if (esPar(numeros[i])) {
                cantidad = cantidad+1;
            }
        }  
        if (cantidad == numeros.length){
            return true;
        }
        else {return false;}
    }

    boolean esPrefijo(String s1, String s2) {
        if (s1.length()>s2.length()) {
            return false;
        }
        
        for(int i=0; i < s1.length(); i++) {
            if (s1.charAt(i)!=s2.charAt(i)) {
                return false;
            }
        }

        return true;
    }

    boolean esSufijo(String s1, String s2) {
        if (s1.length()>s2.length()) {
            return false;
        }
        return esPrefijo(s1, s2.substring(s2.length()-s1.length()));    
    }
}
