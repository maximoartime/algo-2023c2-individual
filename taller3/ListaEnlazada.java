package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private Nodo primero;
    private Nodo ultimo;
    private int longitud;

    private class Nodo {

		Nodo anterior;
        T valor;
        Nodo siguiente;
        Nodo(T v) {valor = v;}
  
    }

    public ListaEnlazada() {
        primero = null;
        ultimo = null;
        longitud = 0;
    }

    public int longitud() {
        return longitud;
        
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        if(longitud == 0) { 
            primero = nuevo; 
            ultimo = nuevo;
        }
        else {
            nuevo.siguiente = primero;
            primero.anterior = nuevo; 
            primero = nuevo; 
        }
        longitud++;   
    }

    public void agregarAtras(T elem) {
         Nodo nuevo = new Nodo(elem);
         if(longitud == 0){
            primero = nuevo; 
            ultimo = nuevo;
         }
         else{
            nuevo.anterior = ultimo;
            ultimo.siguiente = nuevo;
            ultimo = nuevo;
         }
         longitud++;
    }

    public T obtener(int i) {
        Nodo res = primero;
        for(int a = 0; a<i; a++) {
            res = res.siguiente;
        }
        return res.valor;
           
    }

    public void eliminar(int i) {
        Nodo eliminado = primero;
        for(int a = 0; a<i; a++) {
            eliminado = eliminado.siguiente;
        }
        if(eliminado == ultimo){
            ultimo = eliminado.anterior;
        }
        else{
            if(eliminado == primero) {
                primero = eliminado.siguiente;
            }
            else{ 
                Nodo antes = eliminado.anterior;
                Nodo despues = eliminado.siguiente;
                antes.siguiente = despues;
                despues.anterior = antes;
            }
        }
        longitud --;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo modificado = primero;
        for(int a = 0; a<indice; a++) {
            modificado = modificado.siguiente;
        }
        modificado.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> res = new ListaEnlazada<>();
        Nodo nuevoUltimo = new Nodo(null);
        int nuevaLongitud = longitud;
        nuevoUltimo = primero;
        for (int a = 0; a<longitud; a++){
            res.agregarAtras(nuevoUltimo.valor);
            nuevoUltimo = nuevoUltimo.siguiente;
        }
        res.longitud = nuevaLongitud;
        return res;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        Nodo actual = lista.primero;
        while(actual != null) {
            agregarAtras(actual.valor);
            actual = actual.siguiente;
        }
    }
    
    @Override
    public String toString() {
        String res = "[";
        StringBuilder constructor = new StringBuilder(res);
        Nodo contador = primero;
        for (int a = 0; a<longitud; a++) {
            constructor.append(contador.valor.toString());
            constructor.append(", ");
            contador = contador.siguiente;
        }
        constructor.deleteCharAt(constructor.length()-1);
        constructor.deleteCharAt(constructor.length()-1);
        constructor.append("]");
        return constructor.toString();

    }

    private class ListaIterador implements Iterador<T> {
    	int dedito;
        ListaIterador() {dedito = 0;};

        public boolean haySiguiente() {
	        return dedito < longitud;
        }
        
        public boolean hayAnterior() {
	        return dedito > 0 && dedito <= longitud;
        }

        public T siguiente() {
	        int i = dedito;
            dedito++;
            return obtener(i);
        }

        public T anterior() {
                int i = dedito-1;
                dedito --;
                return obtener(i);
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador();
    }

}
