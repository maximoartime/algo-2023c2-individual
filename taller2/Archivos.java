package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] res = new float[largo];
        for (int i = 0; i < largo; i++) {
            res[i] = entrada.nextFloat();
        }
        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float [][] res = new float[filas] [columnas];
        for (int i = 0; i<filas; i++) {
            res[i] = leerVector(entrada, columnas);
        }
        return res;
    }

    void imprimirPiramide(PrintStream salida, int alto) {

        for (int i = 1; i <= alto; i++) {
            String linea = "";
        
            for (int j = 1; j <= alto - i; j++) {
              linea += ' ';
            }
            for (int k = 1; k <= 2 * i - 1; k++) {
              linea += '*';
            }
            for (int j = 1; j <= alto - i; j++) {
              linea += ' ';
            }
            salida.print(linea);
            salida.println();
          }

        }


       
}

